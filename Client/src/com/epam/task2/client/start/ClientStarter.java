package com.epam.task2.client.start;

import com.epam.task2.client.controller.ClientSocket;

/**
 * Created by blooming on 27.12.15.
 */
public class ClientStarter {

    public static void main(String[] args) {
        ClientSocket client = new ClientSocket("localhost", 4800);
        client.start();
    }
}
