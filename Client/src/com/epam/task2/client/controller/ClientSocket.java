package com.epam.task2.client.controller;

import com.epam.tc.task2.bean.Request;
import com.epam.tc.task2.bean.Response;
import com.epam.tc.task2.entity.Gem;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class ClientSocket {
    private static final int ANSWER_SERVER_WAIT_TIME = 1000;

    private String ip;
    private int port;

    private Socket socket;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;

    public ClientSocket(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void start() {
        try {
            socket = new Socket(ip, port);

            System.out.println("Start.");

            InputStream in = socket.getInputStream();
            OutputStream out = socket.getOutputStream();
            inputStream = new ObjectInputStream(in);
            outputStream = new ObjectOutputStream(out);

            Request request = createRequest();
            Response response;

            while (request != null) {
                sendRequestToServer(request);
                response = readAnswerFromServer();
                showResponse(response);
                request = createRequest();
            }
        } catch (IOException | InterruptedException e) {
        }  finally {
            try {
                if ((socket != null) && (sendCloseRequestToServer())) {
                    socket.close();
                    System.out.println("Socket closed.");
                } else {
                    System.out
                            .println("Something happens with socket. Closed.");
                }
            } catch (IOException | InterruptedException e) {
                System.out.println("Connection problem. Socket Closed.");
            }
        }
    }

    private Request createRequest() throws InterruptedException, IOException {
        Request request = new Request();
        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter:\n1-Sax parse\n2-Stax parse\n3-Dom " +
                "parse\n0-Exit\n");
        int i = sc.nextInt();
        switch (i) {
            case 1:
                request.setCommandName("SAX_PARSE");
                return request;
            case 2:
                request.setCommandName("STAX_PARSE");
                return request;
            case 3:
                request.setCommandName("DOM_PARSE");
                return request;
            default:
                return null;
        }
    }

    private Response readAnswerFromServer() throws InterruptedException,
            IOException {
        try {
            Object object = inputStream.readObject();

            while (null == object) {
                Thread.sleep(ANSWER_SERVER_WAIT_TIME);
                object = inputStream.readObject();
            }

            Response response = (Response) object;

            return response;
        } catch (ClassNotFoundException e) {
            System.out.println("Invalid response format");
            return null;
        }
    }

    private void sendRequestToServer(Request request) throws IOException {
        System.out.println("Requested:" + request.getCommandName());
        outputStream.writeObject(request);
    }

    private void showResponse(Response response) {
        System.out.println("Response:");
        if (null != response.getErrorMessage()) {
            System.out.println(response.getErrorMessage());
        } else {
            ArrayList<Gem> gems = response.getXmlContent();
            for (Gem gem : gems) {
                System.out.println(gem.getName());
            }
            System.out.println(gems.size());
        }
    }

    private boolean sendCloseRequestToServer() throws IOException,
            InterruptedException {
        Request request = new Request();
        request.setCommandName("CLOSE");
        outputStream.writeObject(request);
        Response response = readAnswerFromServer();
        if (response.getErrorMessage().equals("Closed")) {
            return true;
        }
        return false;
    }
}
