package com.epam.tc.task2.server.controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by blooming on 26.12.15.
 */
public class XmlServerSocket {

    private ServerSocket server;

    public void start() {
        try {
            server = new ServerSocket(4800);

            while (true) {
                System.out.println("Server started");
                Socket socket = server.accept();

                Thread newClient = new Thread(new ClientManager(socket));
                newClient.start();
                System.out.println("Server closed");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            try {
                if (server != null) {
                    System.out.println("Server closed");
                    server.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
