package com.epam.tc.task2.server.controller.command;

import com.epam.tc.task2.server.controller.command.impl.DomParseCommand;
import com.epam.tc.task2.server.controller.command.impl.SaxParseCommand;
import com.epam.tc.task2.server.controller.command.impl.StaxParseCommand;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by blooming on 27.12.15.
 */
public class CommandHelper {
    private Map<CommandName, ServerCommand> commands = new HashMap<>();

    public CommandHelper(){
        commands.put(CommandName.DOM_PARSE, new DomParseCommand());
        commands.put(CommandName.SAX_PARSE, new SaxParseCommand());
        commands.put(CommandName.STAX_PARSE, new StaxParseCommand());
    }

    public ServerCommand getCommand(String commandName) {
        CommandName command = CommandName.valueOf(commandName);

        return commands.get(command);
    }
}
