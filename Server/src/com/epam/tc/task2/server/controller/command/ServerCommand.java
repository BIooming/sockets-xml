package com.epam.tc.task2.server.controller.command;


import com.epam.tc.task2.bean.Response;

/**
 * Created by blooming on 26.12.15.
 */
public interface ServerCommand {

    Response execute() throws CommandException;
}
