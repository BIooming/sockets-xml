package com.epam.tc.task2.server.controller.command;

/**
 * Created by blooming on 27.12.15.
 */
public enum CommandName {
    STAX_PARSE, SAX_PARSE, DOM_PARSE, CLOSE
}
