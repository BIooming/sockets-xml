package com.epam.tc.task2.server.controller;

import com.epam.tc.task2.bean.Request;
import com.epam.tc.task2.bean.Response;
import com.epam.tc.task2.server.controller.command.CommandException;
import com.epam.tc.task2.server.controller.command.CommandHelper;
import com.epam.tc.task2.server.controller.command.CommandName;
import com.epam.tc.task2.server.controller.command.ServerCommand;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;

/**
 * Created by blooming on 26.12.15.
 */
public class ClientManager implements Runnable {
    private static final int ANSWER_CLIENT_WAIT_TIME = 1000;
    private final static Logger logger = Logger.getLogger("file");

    private CommandHelper commandList = new CommandHelper();
    private Socket socket;

    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;

    public ClientManager(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        {
            try {
                InputStream in = socket.getInputStream();
                OutputStream out = socket.getOutputStream();

                outputStream = new ObjectOutputStream(out);
                inputStream = new ObjectInputStream(in);

                Request request = readRequestFromClient();
                Response response;
                logger.debug("client connection established");
                while (!request.getCommandName().equals(CommandName.CLOSE
                        .toString())) {
                    response = generateResponse(request);
                    sendResponseToClient(response);
                    request = readRequestFromClient();
                }

                sendCloseConfirmationToClient();
                logger.debug("client connection closed");
            } catch (InterruptedException e) {
                logger.error(e);
            } catch (ClassNotFoundException e) {
                logger.error(e);
            } catch (EOFException e) {
                logger.error(e);
            } catch (IOException e) {
                logger.error(e);
            }
        }
    }

    private Request readRequestFromClient() throws IOException,
            InterruptedException, ClassNotFoundException, EOFException {
        Request request = null;
        Object readObject = inputStream.readObject();

        while (null == readObject) {
            Thread.sleep(ANSWER_CLIENT_WAIT_TIME);
            readObject = inputStream.readObject();
        }
        request = (Request) readObject;

        logger.debug("received request:" + request.getCommandName());
        return request;
    }

    private Response generateResponse(Request request) {
        Response response;

        try {
            String commandName = request.getCommandName();
            ServerCommand command = commandList.getCommand(commandName);
            response = command.execute();
        } catch (CommandException e) {
            response = new Response();
            response.setErrorMessage("Server command exception");
            logger.error(e);
        }

        return response;
    }

    private void sendResponseToClient(Response response) throws IOException {
        logger.debug("response sent: " + response.getXmlContent().toString());
        outputStream.writeObject(response);
    }

    private void sendCloseConfirmationToClient() throws IOException {
        Response response = new Response();
        response.setErrorMessage("Closed");
        outputStream.writeObject(response);
        logger.debug("close confirmation sent");
    }
}
