package com.epam.tc.task2.server.controller.command.impl;

import com.epam.tc.task2.bean.Response;
import com.epam.tc.task2.entity.Gem;
import com.epam.tc.task2.server.controller.command.CommandException;
import com.epam.tc.task2.server.controller.command.ServerCommand;
import com.epam.tc.task2.server.service.ServiceException;
import com.epam.tc.task2.server.service.XmlParseService;

import java.util.ArrayList;

/**
 * Created by blooming on 26.12.15.
 */
public class SaxParseCommand implements ServerCommand {

    @Override
    public Response execute() throws CommandException {
        ArrayList<Gem> result;

        try {
            XmlParseService service = new XmlParseService();
            result = service.saxParse();
        } catch (ServiceException e) {
            throw new CommandException("Trouble parsing file");
        }

        Response response = new Response();
        if (null != result) {
            response.setXmlContent(result);
        }else {
            response.setErrorMessage("Can't parse file");
        }

        return response;
    }
}
