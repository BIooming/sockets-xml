package com.epam.tc.task2.server.controller.command;

/**
 * Created by blooming on 26.12.15.
 */
public class CommandException extends Exception {
    private static final long serialVersionUID = 1L;

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Exception ex) {
        super(message, ex);
    }
}
