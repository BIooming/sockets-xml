package com.epam.tc.task2.server.service;


import com.epam.tc.task2.entity.Gem;
import com.epam.tc.task2.server.service.logic.dom.DomHandler;
import com.epam.tc.task2.server.service.logic.sax.SaxHandler;
import com.epam.tc.task2.server.service.logic.stax.StaxHandler;
import com.epam.tc.task2.server.start.PropertiesController;
import org.xml.sax.*;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by blooming on 16.12.15.
 */
public class XmlParseService {
    private static final String XMLFILE = PropertiesController.getInstance().getProperty("xmlfile");
    private static InputStream inputStream;
    private static ArrayList<Gem> gemList;

    public XmlParseService() throws ServiceException {
        try {
            inputStream = new FileInputStream(XMLFILE);
        } catch (FileNotFoundException e) {
            throw new ServiceException("File not found");
        }
    }

    public ArrayList<Gem> saxParse() throws ServiceException {
        try {
            XMLReader reader = XMLReaderFactory.createXMLReader();
            SaxHandler handler = new SaxHandler();

            reader.setContentHandler(handler);
            reader.parse(new InputSource(inputStream));

            reader.setFeature("http://xml.org/sax/features/validation", true);
            reader.setFeature("http://xml.org/sax/features/namespaces", true);
            reader.setFeature("http://xml.org/sax/features/string-interning", true);

            reader.setFeature("http://apache.org/xml/features/validation/schema", false);

            return gemList = handler.getGemList();
        } catch (SAXNotSupportedException e) {
            throw new ServiceException(e.getMessage());
        } catch (IOException e) {
            throw new ServiceException(e.getMessage());
        } catch (SAXNotRecognizedException e) {
            throw new ServiceException(e.getMessage());
        } catch (SAXException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public ArrayList<Gem> staxParse()
            throws ServiceException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();

        try {
            XMLStreamReader reader = inputFactory.createXMLStreamReader(inputStream);
             return gemList = StaxHandler.getGemList(reader);
        } catch (XMLStreamException e) {
            throw new ServiceException(e.toString());
        }
    }

    public ArrayList<Gem> domParse()throws ServiceException{
        try {
            return gemList = DomHandler.getGemList(inputStream);
        } catch (SAXException e) {
            throw new ServiceException(e.getMessage());
        } catch (IOException e) {
            throw new ServiceException(e.getMessage());
        }
    }
}
