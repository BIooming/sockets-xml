package com.epam.tc.task2.server.service.logic.sax;

import com.epam.tc.task2.entity.Gem;
import com.epam.tc.task2.entity.GemCharacteristics;
import com.epam.tc.task2.entity.GemFondTagName;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

/**
 * Created by blooming on 14.12.15.
 */
public class SaxHandler extends DefaultHandler {
    private ArrayList<Gem> gemList = new ArrayList<Gem>();
    private Gem gem;
    private GemCharacteristics gemCharacteristics;
    private StringBuilder text;

    public ArrayList<Gem> getGemList() {
        return gemList;
    }

    public void startDocument() throws SAXException {

    }

    public void endDocument() throws SAXException {

    }

    public void startElement(String uri, String localName,
                             String qName, Attributes attributes) throws SAXException {
        text = new StringBuilder();
        if (qName.equals("gem")) {
            gem = new Gem();
            gem.setId((Integer.parseInt(attributes.getValue("id"))));
        }

        if (qName.equals("parameters")){
            gemCharacteristics = new GemCharacteristics();
        }
    }

    public void characters(char[] buffer, int start, int length) {
        text.append(buffer, start, length);
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        GemFondTagName tagName =
                GemFondTagName.valueOf(qName.toUpperCase());
        switch (tagName) {
            case NAME:
                gem.setName(text.toString());
                break;
            case PRECIOUSNESS:
                gem.setPreciousness(text.toString());
                break;
            case ORIGIN:
                gem.setOrigin(text.toString());
                break;
            case VALUE:
                gem.setValue(Integer.parseInt(text.toString()));
                break;
            case GEM:
                gemList.add(gem);
                gem = null;
                break;
            case COLOR:
                gemCharacteristics.setColor(text.toString());
                break;
            case TRANSPARENCY:
                gemCharacteristics.setTransparency(text.toString());
                break;
            case FACETING:
                gemCharacteristics.setFaceting(Integer.parseInt(text.toString
                        ()));
                break;
            case PARAMETERS:
                gem.setGemCharacteristics(gemCharacteristics);
                gemCharacteristics = null;
                break;
        }
    }
}
