package com.epam.tc.task2.server.service.logic.stax;

import com.epam.tc.task2.entity.Gem;
import com.epam.tc.task2.entity.GemCharacteristics;
import com.epam.tc.task2.entity.GemFondTagName;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.ArrayList;

/**
 * Created by blooming on 14.12.15.
 */
public class StaxHandler {

    private static Gem gem = null;
    private static GemCharacteristics gemCharacteristics = null;
    private static XMLStreamReader reader = null;
    private static ArrayList<Gem> gemList = new ArrayList<>();
    private static GemFondTagName elementName = null;


    public static ArrayList<Gem> getGemList(XMLStreamReader streamReader) throws
            XMLStreamException {
        reader = streamReader;

        while (reader.hasNext()) {
            int type = reader.next();
            processElement(type);
        }

        return gemList;
    }

    private static void processElement(int type) {
        switch (type) {
            case XMLStreamConstants.START_ELEMENT:
                elementName = GemFondTagName.getElementTagName(reader
                        .getLocalName());
                processStartElement(elementName);
                break;

            case XMLStreamConstants.CHARACTERS:
                String text = reader.getText().trim();
                if (text.isEmpty()) {
                    break;
                }
                processCharacters(elementName, text);
                break;

            case XMLStreamConstants.END_ELEMENT:
                elementName = GemFondTagName.getElementTagName(reader.getLocalName());
                processEndElement(elementName);
        }
    }

    private static void processStartElement(GemFondTagName elementName){
        switch (elementName) {
            case GEM:
                gem = new Gem();
                Integer id = Integer.parseInt(reader.getAttributeValue(null, "id"));
                gem.setId(id);
                break;

            case PARAMETERS:
                gemCharacteristics = new GemCharacteristics();
                break;
        }
    }

    private static void processCharacters(GemFondTagName elementName, String text){
        switch (elementName) {
            case NAME:
                gem.setName(text.toString());
                break;
            case PRECIOUSNESS:
                gem.setPreciousness(text.toString());
                break;
            case ORIGIN:
                gem.setOrigin(text.toString());
                break;
            case VALUE:
                gem.setValue(Integer.parseInt(text.toString()));
                break;
            case COLOR:
                gemCharacteristics.setColor(text.toString());
                break;
            case TRANSPARENCY:
                gemCharacteristics.setTransparency(text.toString());
                break;
            case FACETING:
                gemCharacteristics.setFaceting(Integer.parseInt(text.toString
                        ()));
                break;
        }
    }

    private static void processEndElement(GemFondTagName elementName){
        switch (elementName) {
            case GEM:
                gemList.add(gem);
                break;

            case PARAMETERS:
                gem.setGemCharacteristics(gemCharacteristics);
                break;
        }
    }
}
