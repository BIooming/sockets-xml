package com.epam.tc.task2.server.service.logic.dom;

import com.epam.tc.task2.entity.Gem;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by blooming on 16.12.15.
 */
public class DomHandler {

    public static ArrayList<Gem> getGemList(InputStream inputStream) throws SAXException, IOException {
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(inputStream));
        Document document = parser.getDocument();
        Element root = document.getDocumentElement();

        ArrayList<Gem> gemList = new ArrayList<Gem>();
        NodeList gemNodes = root.getElementsByTagName("gem");

        Gem gem = null;
        for (int i = 0; i < gemNodes.getLength(); i++) {
            gem = new Gem();
            Element gemElement = (Element) gemNodes.item(i);
            gem.setId(Integer.parseInt(gemElement.getAttribute("id")));
            gem.setName(getSingleChild(gemElement,
                    "name").getTextContent().trim());

            gemList.add(gem);
        }
        return gemList;
    }

    private static Element getSingleChild(Element element, String childName){
        NodeList nodeList = element.getElementsByTagName(childName);
        Element child = (Element) nodeList.item(0);
        return child;
    }
}
