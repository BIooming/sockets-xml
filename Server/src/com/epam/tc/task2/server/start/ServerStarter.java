package com.epam.tc.task2.server.start;

import com.epam.tc.task2.server.controller.XmlServerSocket;

/**
 * Created by blooming on 26.12.15.
 */
public class ServerStarter {

    public static void main(String[] args) {
        XmlServerSocket server = new XmlServerSocket();
        server.start();
    }
}
