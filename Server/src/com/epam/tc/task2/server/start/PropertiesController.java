package com.epam.tc.task2.server.start;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by blooming on 27.12.15.
 */
public class PropertiesController {
    private static Properties properties = new Properties();
    private static final PropertiesController instance = new PropertiesController();

    private PropertiesController() {
        InputStream input = null;

        try {
            input = new FileInputStream("config.prop");

            properties.load(input);

        } catch (FileNotFoundException e) {
            System.out.println("Error in loading properties file ");
        } catch (IOException e) {
            System.out.println("Error in loading properties file");
        } finally {
            if (input != null){
                try{
                    input.close();
                }catch (IOException e){
                    System.out.println("Error in closing properties file");
                }
            }
        }
    }

    public static PropertiesController getInstance() {
        return instance;
    }

    public String getProperty(String key){
        return properties.getProperty(key);
    }
}
