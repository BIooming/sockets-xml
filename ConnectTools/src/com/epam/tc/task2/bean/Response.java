package com.epam.tc.task2.bean;

import com.epam.tc.task2.entity.Gem;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by blooming on 27.12.15.
 */
public class Response implements Serializable {
    private String errorMessage;
    private ArrayList<Gem> xmlContent;

    public ArrayList<Gem> getXmlContent() {
        return xmlContent;
    }

    public void setXmlContent(ArrayList<Gem> xmlContent) {
        this.xmlContent = xmlContent;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
