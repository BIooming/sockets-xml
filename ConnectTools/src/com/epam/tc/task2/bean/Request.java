package com.epam.tc.task2.bean;

import java.io.Serializable;

/**
 * Created by blooming on 27.12.15.
 */
public class Request implements Serializable {
    private String commandName;

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }
}
