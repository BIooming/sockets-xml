package com.epam.tc.task2.entity;

import java.io.Serializable;

/**
 * Created by blooming on 14.12.15.
 */
public enum GemFondTagName implements Serializable {
    GEMS, GEM, NAME, PRECIOUSNESS, ORIGIN, PARAMETERS, COLOR, TRANSPARENCY,
    FACETING, VALUE;

    public static GemFondTagName getElementTagName(String element) {
        switch (element) {
            case "gems":
                return GEMS;
            case "gem":
                return GEM;
            case "name":
                return NAME;
            case "preciousness":
                return PRECIOUSNESS;
            case "origin":
                return ORIGIN;
            case "parameters":
                return PARAMETERS;
            case "color":
                return COLOR;
            case "transparency":
                return TRANSPARENCY;
            case "faceting":
                return FACETING;
            case "value":
                return VALUE;
            default:
                throw new EnumConstantNotPresentException(GemFondTagName.class,
                        element);
        }
    }
}
