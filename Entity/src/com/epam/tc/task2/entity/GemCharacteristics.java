package com.epam.tc.task2.entity;

import java.io.Serializable;

/**
 * Created by blooming on 14.12.15.
 */

public class GemCharacteristics implements Serializable {
    private String color;
    private String transparency;
    private int faceting;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTransparency() {
        return transparency;
    }

    public void setTransparency(String transparency) {
        this.transparency = transparency;
    }

    public int getFaceting() {
        return faceting;
    }

    public void setFaceting(int faceting) {
        this.faceting = faceting;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (null == object) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }

        GemCharacteristics characteristics = (GemCharacteristics) object;
        if (!color.equals(characteristics.color)) {
            return false;
        }
        if (!transparency.equals(characteristics.transparency)) {
            return false;
        }
        if (faceting != characteristics.faceting) {
            return false;
        }

        return true;
    }

    public String toString() {
        return getClass().getName() + "@ color: " + color +
                ", transparency: " + transparency +
                ", faceting: " + faceting;
    }

    public int hashCode() {
        int hash = 0;

        if (color != null) {
            hash = hash + color.hashCode();
        }
        if (transparency != null) {
            hash = hash + transparency.hashCode();
        }

        hash = hash + 31 * faceting;
        return hash;
    }
}
