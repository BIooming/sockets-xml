package com.epam.tc.task2.entity;

import java.io.Serializable;

/**
 * Created by blooming on 14.12.15.
 */

public class Gem implements Serializable {
    private String name;
    private String preciousness;
    private String origin;
    private int value;
    private int id;
    private GemCharacteristics GemCharacteristics;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreciousness() {
        return preciousness;
    }

    public void setPreciousness(String preciousness) {
        this.preciousness = preciousness;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public GemCharacteristics getGemCharacteristics() {
        return GemCharacteristics;
    }

    public void setGemCharacteristics(GemCharacteristics GemCharacteristics) {
        this.GemCharacteristics = GemCharacteristics;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (null == object) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }

        Gem gem = (Gem) object;
        if (!name.equals(gem.name)) {
            return false;
        }
        if (!preciousness.equals(gem.preciousness)) {
            return false;
        }
        if (!origin.equals(gem.origin)) {
            return false;
        }
        if (value != gem.value) {
            return false;
        }
        if (id != gem.id) {
            return false;
        }
        if (!GemCharacteristics.equals(gem.GemCharacteristics)) {
            return false;
        }

        return true;
    }

    public String toString() {
        return getClass().getName() + "@name: " + name +
                ", preciousness: " + preciousness +
                ", origin: " + origin +
                ", value: " + value +
                ", id: " + id +
                ", parameter set: " + GemCharacteristics.toString();
    }

    public int hashCode() {
        int hash = 0;
        if (name != null) {
            hash = hash + name.hashCode();
        }
        if (preciousness != null) {
            hash = hash + preciousness.hashCode();
        }
        if (origin != null) {
            hash = hash + origin.hashCode();
        }
        if (GemCharacteristics != null) {
            hash = hash + GemCharacteristics.hashCode();
        }
        hash = hash + value * 31 + id * 31;
        return hash;
    }
}
